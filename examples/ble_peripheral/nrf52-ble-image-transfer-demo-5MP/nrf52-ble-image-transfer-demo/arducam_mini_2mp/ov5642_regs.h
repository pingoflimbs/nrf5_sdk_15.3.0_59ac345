#ifndef OV5642_REGS_H
#define OV5642_REGS_H
#include "ArduCAM.h"
//#include <avr/pgmspace.h>

#define OV5642_CHIPID_HIGH 0x300a
#define OV5642_CHIPID_LOW 0x300b

extern const struct sensor_reg ov5642_RAW[];
extern const struct sensor_reg OV5642_1280x960_RAW[];
extern const struct sensor_reg OV5642_1920x1080_RAW[];
extern const struct sensor_reg OV5642_640x480_RAW[];
extern const struct sensor_reg ov5642_320x240[];
extern const struct sensor_reg ov5642_640x480[];
extern const struct sensor_reg ov5642_1280x960[];
extern const struct sensor_reg ov5642_1600x1200[];
extern const struct sensor_reg ov5642_1024x768[];
extern const struct sensor_reg ov5642_2048x1536[];
extern const struct sensor_reg ov5642_2592x1944[];
extern const struct sensor_reg ov5642_dvp_zoom8[];
extern const struct sensor_reg OV5642_QVGA_Preview[];
extern const struct sensor_reg OV5642_JPEG_Capture_QSXGA[];
extern const struct sensor_reg OV5642_1080P_Video_setting[];
extern const struct sensor_reg OV5642_720P_Video_setting[];

#endif

